#! /usr/bin/env python3.8
import random, sys

vowels =     ['a','e','i','o','u','y']
consonants = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z']

if len(sys.argv) >= 2: 
    if sys.argv[1] in ('-l', '--length'):
        try:
            length = int(sys.argv[2])
        except:
            print('\033[32mError:\033[0m length is now 8')
            length = 8
else:
    length = 8

name = ''

for i in range(length):
    if i%2 == 0:
        name += (random.choice(consonants) if i != 0 else random.choice(consonants).upper())
    else:
        name += (random.choice(vowels))
print(name)
print("random.choice(consonants)")
