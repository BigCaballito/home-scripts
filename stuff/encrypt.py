#!/usr/bin/env python3
import hashlib
import random
import argparse
from sys import stdin

def encrypt(data: str, pwd: str):
    result = R_TYPE
    ped = hashlib.sha512(bytes(pwd, encoding='utf8')).hexdigest()
    random.seed(pwd)
    for char in data:
            idx = ORD(char)
            for it in range(len(ped)):
                    if it == 0:
                            iOut = idx^ (ord(ped[it])^random.randint(0,127))
                    else:
                            iOut = iOut^ (ord(ped[it])^random.randint(0,127))
                    if it == len(ped)-1:
                            result += CHR(iOut)
    return result

#'''
parser = argparse.ArgumentParser(description='Data or File Encryption and Decryption')

parser.add_argument('file', type=str, help='File to encrypt')
parser.add_argument('pwd', type=str, help='Password')
parser.add_argument('-o', '--out', type=str, help='Output file, default is stdout')
parser.add_argument('-b', '--binary', action='store_true', help='Use for bianry files, like images or executables')

args = parser.parse_args()

if args.binary:
    F_SUFFIX = 'b'
    ORD = lambda x:x
    CHR = lambda y:bytes(str(y), 'utf8')
    R_TYPE = b''
else:
    F_SUFFIX = ''
    ORD = lambda x:ord(x)
    CHR = lambda y:chr(y)
    R_TYPE = ''

if args.file == '-':
    sData = stdin.read()
else:
    with open(args.file, 'r'+F_SUFFIX) as f:
        sData = f.read()

sPass = args.pwd
if args.out:
    with open(args.out, 'w+'+F_SUFFIX) as f:
        f.write(encrypt(sData, sPass))
else:
    print(encrypt(sData, sPass))
#'''
