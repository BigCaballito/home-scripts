#!/usr/bin/perl
use strict;
use warnings;

my $csv = `curl https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv`;
my @kan = $csv =~ /(2020-\d\d-\d\d\,Ka.*)/g;
print "$_\n" for @kan;
