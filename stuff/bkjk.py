#!/usr/bin/python
import random
from sys import argv

class Card:
    def __init__(self, suit, rank):
        if suit in (('H','C','S','D')):
            self.suit = suit
        if rank in (('A',2,3,4,5,6,7,8,9,10,'J','Q','K')):
            self.rank = rank
        self.vals = (self.rank if type(self.rank) == int else self.value(self.rank),
                     self.rank if type(self.rank) == int else self.value(self.rank,p=1))
    def __str__(self):
        suits = {'D':'\u2666', 'H':'\u2665', 'C':'\u2663', 'S':'\u2660'}
        return '{0}{1}'.format(self.rank,suits[self.suit])
    def value(self, r : str, p = None):
        if r in 'KQJ':
            return 10
        else:
            return 11 if p == None else 1

class Player:
    has_blackjack = False
    is_bust = False
    money = 5_000
    current_bet = 0
    current_cards = [None,None] #type: list(tuple)
    current_move = 'stay'
    def get_bet(self, round = -1):
        questions = ('How much do you want to bet? ', 'Raise your bet? ')
        while True:
            try:
                if round == 0:
                    self.current_bet = (bet := int(input(questions[round]) or 0))
                else:
                    self.current_bet += (bet := int(input(questions[round]) or 0))
                if self.current_bet > self.money:
                    print('That\'s too much money')
                    self.current_bet -= bet
                elif bet < -(self.current_bet - bet):
                    print('That\'s too little money')
                    self.current_bet -= bet
                else:
                    break
            except ValueError:
                break
    def get_move(self):
        move = input('Hit or Stay? ')
        while move not in ('Hit', 'hit', 'Stay', 'stay', 'H', 'h', '+', 'S', 's', '-'):
            move = input(f'I\'m sorry, I don\'t understand {move}\n please choose Hit or Stay')
        self.current_move = 'hit' if move in ('Hit', 'hit', 'h', '+') else 'stay'
    def check_cards(self, prt = True):
        sum = 0
        for card in self.current_cards:
            sum += card.vals[0]
            if prt: print(f'|{str(card)}| ', end='')
        print('\n')
        if sum > 21:
            sum = 0
            for card in self.current_cards:
                sum += card.vals[1]
            if sum > 21:
                self.is_bust = True
        elif sum == 21:
            self.has_blackjack = True
        else:
            return sum
        
#END

class Game:
    def __init__(self, player=None, decks=1):
        if not player:
            self.player = Player()
        else:
            self.player = player
        self.cards = [Card(s,r) for s in 'HCSD' for r in ('A',2,3,4,5,6,7,8,9,10,'J','Q','K')]*decks
    bet = 0
    discard = []
    current_cards = [None,None]
    
    def new_card(self):
        random.shuffle(self.cards)
        self.discard.append(card := self.cards.pop())
        return card
    def deal(self):
        for i in range(2):
            self.player.current_cards[i] = self.new_card()
            self.current_cards[i] = self.new_card()
    def show_card(self):
        print(f'--- DEALER ---\n|{str(self.current_cards[0])}| |##|\n---- YOU ----')
        
    def play(self):
        self.player.get_bet(0)
        self.bet = self.player.current_bet
        self.deal()
        self.show_card()
        self.player.check_cards()
        self.player.get_bet()
        self.bet = self.player.current_bet
        self.player.get_move()
        while self.player.current_move == 'hit':
            self.player.current_cards.append(self.new_card())
            self.player.check_cards()
            if self.player.is_bust:
                break
            elif self.player.has_blackjack:
                break
            self.player.get_move()
        self.player.check_cards(False)
        print('Dealer ==> ',end='')
        for card in self.current_cards:
            print(f'|{str(card)}| ',end='')
        print()
        if self.player.is_bust:
            print(f'You lost ${self.player.current_bet}.')
            self.player.money -= self.player.current_bet
            print(f'You have ${self.player.money}')
        elif self.player.has_blackjack:
            print(f'You WON ${self.player.current_bet * 2}!!!')
            self.player.money += self.bet
            print(f'You have ${self.player.money}!!')
        else:
            playersum = self.player.check_cards(False)
            selfsum = 0
            for card in self.current_cards:
                selfsum += card.vals[0]
            if selfsum > 21:
                breakpoint()
                selfsum = 0
                for card in self.current_cards:
                    selfsum += card.vals[1]
            if (selfsum > 21 or playersum > selfsum):
                print(f'You WON ${self.player.current_bet * 2}!!!')
                self.player.money += self.bet
                print(f'You have ${self.player.money}!!')
            elif selfsum > playersum:
                print(f'You lost ${self.player.current_bet}')
                self.player.money -= self.player.current_bet
                print(f'You have ${self.player.money}')
            else:
                print('You tied')
        self.player.current_bet = 0
        self.player.is_bust = False
        self.player.has_blackjack = False
        self.player.current_cards = [None,None]
        self.bet = 0
        self.current_cards = [None, None]
        
#END
def play(game: Game):
    while True:
        try:
            game.play()
            if game.player.money <= 0:
                print('Sorry, You ran out of money.\nBetter luck next time.')
                break
        except (IndexError):
            print(f'\nGame Over, the deck ran out. You have ${game.player.money}.\
                    You made a profit of ${game.player.money - 5_000}')
            break
        except (KeyboardInterrupt):
            print(f'\nGame Over. You have ${game.player.money}.\
                    You made a profit of ${game.player.money - 5_000}')
            break
    with open('/home/enoch/games/bj_high.scr', 'a+') as scorefile:
        scorefile.write(f"{input('Type your name: ').strip()}\t{game.player.money:07}\n")

print('Blackjack!\nC-c : quit\n')
if len(argv) > 1:
    decks = -int(argv[1])
else:
    decks = 1
game = Game(decks=decks)
print(f'You have ${game.player.money}')
play(game)

