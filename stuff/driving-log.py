#!/usr/bin/python3
import tkinter as tk
import os
import re
from datetime import date

now = str(date.today()).split('-')
now = now[1]+'/'+now[2]+'/'+now[0]

logpath = os.path.expanduser('~') + '/Desktop/driving_log.txt'
logfile = open(logpath, 'a+')

def sumHours(lfile):
	lfile.seek(0)
	lines = lfile.readlines()
	times = []
	for line in lines:
		times += list(filter(None, re.split('\t| ', line)[1:]))
	totalTime = 0
	for time in times:
		print(time)
		totalTime += 0.5 if time == '30m\n' else 1
	return totalTime

def hourAdd():
	logfile.write(now + '\t\t1h\n')
	info.set(sumHours(logfile))
	
def halfHourAdd():
	logfile.write(now + '\t\t30m\n')
	info.set(sumHours(logfile))

root = tk.Tk()

info = tk.StringVar()
info.set(sumHours(logfile))
infoLabel = tk.Label(root, textvariable=info)
infoLabel.grid(row=0, column=0, columnspan=2)

hButton = tk.Button(root, text='One Hour', command=hourAdd)
mButton = tk.Button(root, text='Half Hour', command=halfHourAdd)
hButton.grid(row=1, column=0)
mButton.grid(row=1, column=1)

root.mainloop()
logfile.close()