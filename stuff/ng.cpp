#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

#define LENGTH 400

int main(int argc, char* argv[])
{
        std::srand(std::time(nullptr));

        unsigned long int len;
        if (argc > 1)
                len = std::atoi(argv[1]);
        else
                len = LENGTH;

        const char consonants[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};
        const char vowels[] = {'a','e','i','o','u','y'};
        std::string name = std::string();
        for (int i = 0; i != len; ++i)
        {
                if (i % 2 != 0)
                        name += (vowels[rand() % 6]);
                else
                        name += (consonants[rand() % 20]);
        }
        name[0] = std::toupper(name[0]);
//        std::cout << "Good day, how are you? ";
  //      std::cout << "My name is ";
        std::cout << name;
    //    std::cout << ".\nWhat is your name?" << '\n';
        return 0;
}
