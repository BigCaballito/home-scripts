# My Random Scripts
While there are quite a few scripts, not all of them are useful. Some are for playing, some are for working (./tools), and a few of them are very specific to my machine. Enjoy!

## List of scripts

README.md
```
./perlTests:
        regex.pl
        test0.pl
        test1.pl
        test2.pl
./rofi:
        winx.py
./stuff:
        bkjk.py         A fun blackjack game in the terminal
        covid.pl        Messing around with the nytimes' covid lists
        dl-esv.py       A tool(ish) for pirating the ESV Bible
        driving-log.py  Logs stuff
        encrypt.py      Lossy encryption tool
        hiscor.sh
        manage.csv
        name_gen.py
        NG
        ng.cpp
        school.py
./tools:
        ascii           prints ascii table
        chk-rbt         checks if a reeboot is advisable
        ckyst           starts conky
        kde-lock        locks the kde plasma taskbar
        kde-ulock       ^lock^unlock
        mt-sv-tp        [WIP] adds texture pack to a MT game
        music
        slock
        vol
        weather
```
