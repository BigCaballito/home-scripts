#!/usr/bin/python
# a rofi mode written in python
import sys
import os

CONFIG_PATH = os.path.expanduser('~') + '/.config'
if not os.path.isdir(CONFIG_PATH + '/rofi-winx'):
    os.mkdir(CONFIG_PATH + '/rofi-winx')
    CONFIG_PATH += '/rofi-winx'
    f=open(CONFIG_PATH + '/config', 'w+')
    f.close()
    CONFIG = CONFIG_PATH + '/config'
else:
    CONFIG = CONFIG_PATH + '/rofi-winx/config'

with open(CONFIG, 'r') as conf:
    entries = {}
    for line in conf.readlines():
        if line[-1] == '\n':
            line = line[0:-1]
        entry = line.split(';')
        entries[entry[0]] = entry[1]

if len(sys.argv) == 1:
    print('\0message\x1f&')
    print('\0prompt\x1f>>> ')
    keys = list(entries.keys())
    for i in range(len(keys)):
        print(f'{i}: {keys[i]}')
else:
    Exec = entries[sys.argv[1].split(' ')[1]]
#    sys.stderr.write(Exec)
    if Exec[0:2] == '!!':
        os.system(f'{Exec[2:]} 1>/dev/null 2>&1 &')
    else:
        os.system(f'gtk-launch {Exec} 1>/dev/null 2>&1 &')

