#!/usr/bin/perl
use strict;
use warnings;

my @disiples = (
    'Peter',
    'Andrew',
    'James',
    'John',
    'Philip',
    'Bartholomew',
    'Thomas',
    'Matthew',
    'James A',
    'Thaddaeus',
    'Judas',
    'Simon',
    'Judas I'
);
my @itribes = (
    'Judah',
    'Levi',
    'Dan',
    'Naphtali',
    'Benjamin',
    'Ephraim',
    'Manassah',
    'Gad',
    'Reuben',
    'Zebulun',
    'Asher',
    'Simeon',
    'Issachar'
);
my @all = (@disiples,@itribes);
print "@all\n\n";
for ( @all ) {
    print m/([PAJBTMS](?!s).(?!v|n).(?!ah|eo).*)/;
    print ", ";
}
print "\n#####################################\n";
for ( @all ) {
    print m/((?:Judah|L|D|N|Be|E|Man|G|R|Z|As|Sime|Is).*)/;
    print ", ";
}
