#!/usr/bin/env perl

use strict;
use warnings;

if (@ARGV != 2) {
    print STDERR "Incorrect args.\n";
    exit;
}

my $fn = $ARGV[0];
my $ln = reverse $ARGV[1];

print $fn.' '.$ln."\n";

